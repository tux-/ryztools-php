Welcome to Ryztools php
================
A Ryzom php library in development.

> **Highlights:**

> - Api connections.

Requirements
----------------
- Any operating system.
- php 5.6
- [Gimlé 5](https://github.com/Gimle/gimle5).

Join in
-------
Join in via our irc channel **#Gimle** @ freenode.
