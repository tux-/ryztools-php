<?php
namespace gimle\ryzom;

use gimle\Config;
use gimle\System;

if (Config::exists('nimetu')) {
	System::autoloadRegister(Config::get('nimetu') . 'ryzom_extra/lib/', false);
	System::autoloadRegister(Config::get('nimetu') . 'ryzom_weather/lib/', false);
}
