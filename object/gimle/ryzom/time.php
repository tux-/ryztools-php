<?php
namespace gimle\ryzom;

use function gimle\d;

class Time
{
	const SPRING = 0;
	const SUMMER = 1;
	const AUTUMN = 2;
	const WINTER = 3;

	const DAWN = 0;
	const DAY = 1;
	const TWILIGHT = 2;
	const NIGHT = 3;

	const HOUR_TICKS = 1800;
	const DAY_HOURS = 24;
	const SEASON_DAYS = 90;
	const MONTH_DAYS = 30;
	const CYCLE_MONTHS = 12;
	const JY_CYCLES = 4;
	const WEEK_DAYS = 6;
	const CYCLE_SEASON = 4;

	const SEASON_OFFSET_TICKS = 61 * self::DAY_HOURS * self::HOUR_TICKS;

	const START_JY = 2568;

	/* Seems like the tick runs about five - eight minutes slow during a season. */
	/* So we need to add five minutes. (Better to be early than late). */
	const SEASON_MINUTES_OFFSET = 5;

	/* Helpers */
	const CYCLE_DAYS = self::CYCLE_MONTHS * self::MONTH_DAYS;
	const JY_DAYS = self::CYCLE_DAYS * self::JY_CYCLES;
	const JY_MONTHS = self::CYCLE_MONTHS * self::JY_CYCLES;
	const SEASON_TICKS = self::HOUR_TICKS * self::DAY_HOURS * self::SEASON_DAYS;
	const JY_TICKS = self::SEASON_TICKS * self::JY_CYCLES;

	private $tick;

	public function __construct ()
	{
		$time = simplexml_load_string((new Api())->time());

		$serverTick = (int)$time->xpath('/shard_time/server_tick')[0];
		$created = (int)$time->xpath('/shard_time/cache/@created')[0];

		$now = (int)round(microtime(true) * 1000);
		$createdOffset = (($now - ($created * 1000)) / 1000);
		$this->tick = $serverTick + ($createdOffset * 10);
	}

	public function time ()
	{
		$timeInHours = $this->tick / self::HOUR_TICKS;
		return abs(fmod($timeInHours, self::DAY_HOURS));
	}

	public function nextSeasonChange ()
	{
		return (int)round(microtime(true) + $this->secsToSeasonChange());
	}

	public function dayOfSeason ()
	{
		$timeInHours = ($this->tick - self::SEASON_OFFSET_TICKS) / self::HOUR_TICKS;
		$day = $timeInHours / self::DAY_HOURS;
		return (int)fmod($day, self::SEASON_DAYS);
	}

	public function dayOfWeek ()
	{
	}

	public function month ()
	{
	}

	public function year ()
	{
	}

	public function season ()
	{
		$ticks = (($this->tick - self::SEASON_OFFSET_TICKS) % self::JY_TICKS);
		return (int)floor($ticks / self::SEASON_TICKS);
	}

	public function seasonName ()
	{
		$season = $this->season();
		if ($season === 0) {
			return 'Spring';
		} elseif ($season === 0) {
			return 'Summer';
		} elseif ($season === 0) {
			return 'Autumn';
		}
		return 'Winter';
	}

	public function tick ()
	{
		return $this->tick;
	}

	public function hourTick ()
	{
		return (int)(floor($this->tick) - (floor($this->tick) % self::HOUR_TICKS));
	}

	public function nextHourTick ()
	{
		return ($this->hourTick() + self::HOUR_TICKS);
	}

	public function tickOfDay ()
	{
		return (int)$this->tick % (HOUR_TICKS * DAY_HOURS);
	}

	public function secsToSeasonChange ()
	{
		$apiMinutesLeftInSeason = 6480 - ($this->dayOfSeason() * 72) - (($this->time() * 3));
		$apiMinutesLeftInSeason += (($apiMinutesLeftInSeason / 6480) * self::SEASON_MINUTES_OFFSET);

		return $apiMinutesLeftInSeason * 60;
	}

	public function nextSeason ()
	{
		$s = $this->season();
		if ($s === self::SPRING) {
			return self::SUMMER;
		}
		if ($s === self::SUMMER) {
			return self::AUTUMN;
		}
		if ($s === self::AUTUMN) {
			return self::WINTER;
		}
		return self::SPRING;
	}

	public function dayNight ()
	{
		return $this->partOfDay() === self::NIGHT ? self::NIGHT : self::DAY;
	}

	public function partOfDay ()
	{
		$h = $this->time();
		if ($h >= 22) {
			return self::NIGHT;
		}
		if ($h >= 20) {
			return self::TWILIGHT;
		}
		if ($h >= 6) {
			return self::DAY;
		}
		if ($h >= 3) {
			return self::DAWN;
		}
		return self::NIGHT;
	}

	public function nextPartOfDay ()
	{
		$p = $this->partOfDay();
		if ($p === self::DAWN) {
			return self::DAY;
		}
		if ($p === self::DAY) {
			return self::TWILIGHT;
		}
		if ($p === self::TWILIGHT) {
			return self::NIGHT;
		}
		return self::DAWN;
	}
}
