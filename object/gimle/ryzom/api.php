<?php
namespace gimle\ryzom;

use const gimle\TEMP_DIR;

use gimle\rest\FetchCache;

class Api extends FetchCache
{
	const BASE = 'http://api.ryzom.com/';

	private $allowFetching;

	public function __construct ($allowFetching = true)
	{
		parent::__construct();

		$this->connectionTimeout(1);
		$this->resultTimeout(4);

		$this->allowFetching = $allowFetching;
	}

	public function time ($format = 'xml')
	{
		if ($this->allowFetching === true) {
			$ttl = 600;
		} else {
			$ttl = false;
		}
		$return = $this->expect(self::XML)->expire('/shard_time/cache/@expire')->query(self::BASE . 'time.php?format=' . $format, $ttl);
		return $return['reply'];
	}

	public function guild (...$keys)
	{
		if (empty($keys)) {
			return false;
		}

		if ($this->allowFetching === true) {
			$ttl = 600;
		} else {
			$ttl = false;
		}

		if (count($keys) === 1) {
			$return = $this->expect(self::XML)->expire('/ryzomapi/guild/@cached_until')->query(self::BASE . 'guild.php?apikey=' . $keys[0], $ttl);
		} else {
			$return = $this->expect(self::XML)->expire('/ryzomapi/guild/@cached_until')->query(self::BASE . 'guild.php?apikey[]=' . implode('&apikey[]=', $keys), $ttl);
		}
		return $return;
	}

	public function character (...$keys)
	{
		if (empty($keys)) {
			return false;
		}
		if (count($keys) === 1) {
			return $this->query(self::BASE . 'character.php?apikey=' . $keys[0]);
		}
		return $this->query(self::BASE . 'character.php?apikey[]=' . implode('&apikey[]=', $keys));
	}

	public function guilds ()
	{
		return $this->query(self::BASE . 'guilds.php');
	}
}
